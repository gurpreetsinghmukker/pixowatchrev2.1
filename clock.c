#include <stm32f091xc.h>
#include "clock.h"

int config_sys_clock()
{
	/*With Current settings the clock is set at 48MHz
		(HSI(8MHz PLL Input))*(1/2(Predivision factor of 0.5))*12((PLL Multiplier))=48MHz
	*/ 
	FLASH->ACR|=FLASH_ACR_LATENCY;//Set the flash latency to 1 Wait States
	RCC->CR&=(~RCC_CR_PLLON);//PLL off
	RCC->CFGR|=RCC_CFGR_PLLSRC_HSI_PREDIV;//set HSI as input to PLLSRC
	RCC->CFGR2=0x01;//Setting PREDIV to 1
	RCC->CFGR|=(RCC_CFGR_PLLMUL_1|RCC_CFGR_PLLMUL_3);//setting PLL multiplier to 12
	RCC->CFGR&=~(RCC_CFGR_PLLMUL_0|RCC_CFGR_PLLMUL_2);//setting PLL multiplier to 12
	
	RCC->CR|=RCC_CR_PLLON;//PLL On
	
	while(((RCC->CR)& RCC_CR_PLLRDY)==0)// Wait for the PLL to get ready
	{
		if((RCC->CR&RCC_CR_PLLRDY)>0)
		{
			break;
		}
	}
	
	RCC->CFGR|=RCC_CFGR_SW_1;
	RCC->CFGR&=~RCC_CFGR_SW_0;
	
	if((RCC->CFGR&RCC_CFGR_SWS_1)>0)//
	{
		return 1;
	}
	
	return 0;
}

//void config_MCO(void)
//{
//	RCC->CFGR|=RCC_CFGR_MCOPRE_DIV128;
//	RCC->CFGR|=RCC_CFGR_PLLNODIV;
//	RCC->CFGR|=RCC_CFGR_MCO_2;
//	
//}


