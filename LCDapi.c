/* The Screen is divided into 4 rows and 11 columns. The 2-D array asciiLine[][] is used to store the characters to be displayed in 'char' format.
To use the api, first update the characters using row number and column number as parameters in the asciiLine array using updateCharacter() function, 
then use the refreshRow() function with row number as parameter to refresh that particular row.   
*/
#include "LCDapi.h"
#include "LPM013M126A.h"
#include "Fonts.h"
#include "uart1.h"


volatile uint8_t field_num=0;
/*Array storing the info to be displayed on the screen. The function that drwas the lines, refers to this array*/
const uint8_t underline[8][5]={{0,1,9,1,7},{1,0,2,1,12},{1,4,5,1,31},{1,9,10,0,30},{2,0,1,0,12},{2,3,4,0,59},{2,6,7,0,59},{2,9,10,0,1}};//{ROW_NUMBER,ROW_STARTING_POINT,ROW_ENDING_POINT,COUNTER_STARTING_POINT,COUNTER_ENDING POINT) }
extern uint8_t setting_time;

char asciiLine[4][11]={
{" Wednesday "},
{"MAY 14,2019"},
{"12:12:12 PM"},
{"[###]- 85% "}};

char asciiLine1[4][11]={
{" Wednesday "},
{"MAY 14,2019"},
{"12:12:12 PM"},
{"[###]- 85% "}};


static char charUnderline[4][11]={
{"00000000000"},
{"00000000000"},
{"00000000000"},
{"00000000000"}};

/*2D array storing the actual bit values of the indiviadual pixels of the screem*/
static uint16_t line[V_LINES_IN_BLOCK][BLOCKS_IN_H_LINE]={0};

/* Array of strings used by update_LCD_time_date() function to update asciiLine array*/


struct screen_modified{
	
	uint8_t row_num[4];
}	;

// shared struct between update_LCD_time_date() and refresh_row(uint8_t rowNum) function to signal if the data in asciiLine array is modified or not. 
static struct screen_modified scr_mod_1={{1,1,1,1}};
struct screen_modified* ptr_scr_mod_1=&scr_mod_1;

void clear_charunderline(void)
{
	for(uint8_t j=0; j<4;j++)
	{
				
		for(uint8_t i=0; i<11; i++)
			{
				charUnderline[j][i]='0';
			}
			
  }
	
}

void modify_char_underline(uint8_t field_num)
{
	clear_charunderline();
	for(uint8_t i=underline[field_num][1];i<=underline[field_num][2];i++)
	{
		charUnderline[underline[field_num][0]][i]='1';
	}
		
}


void draw_screen(void)
{
		refresh_row(3);
		refresh_row(1);	
		refresh_row(2);		
		refresh_row(4);
}

void refresh_row(uint8_t rowNum)
{
	
	rowNum=rowNum-1;
//	if(ptr_scr_mod_1->row_num[rowNum]==1)
//	{
		make_row_array(rowNum);
		uint16_t * ptrLine;

		for(uint16_t i=1+44*(rowNum);i<=(44*((rowNum)+1));i++)
		{
			ptrLine=&line[i-44*(rowNum)][0];
			LCD_write_line(i,ptrLine);
		} 
//	}	
	
	ptr_scr_mod_1->row_num[rowNum]=0;
			
}

void update_character(char character, uint8_t rowNum, uint8_t BlockNum)
{
	asciiLine[_ROW(rowNum)][_BLOCK(BlockNum)]=character;
	
}




void make_row_array(uint8_t rowNum)
{

	
	for(uint8_t o=(0);o<((V_LINES_IN_BLOCK-2)/V_SCALE_FACTOR);o++)
        {
            for(uint8_t n=0;n<(V_SCALE_FACTOR);n++)
            {
                for(uint8_t m=0;m<BLOCKS_IN_H_LINE;m++)
                {
									
                    for(uint8_t l=0;l<FONT_WIDTH;l++)
                    {
												
											if(setting_time==1)
											{
												if((newFont[asciiLine1[rowNum][m]-32][o]&(0x1<<(FONT_WIDTH-(l+1))))==0)
												{
														line[V_SCALE_FACTOR*o+n+2][m]=(line[5*o+n+2][m]<<2);
												}
										 
												else
												{
														line[V_SCALE_FACTOR*o+n+2][m]=((line[5*o+n+2][m]<<2)|3);
												}
											}
											
											else
												{
												if((newFont[asciiLine[rowNum][m]-32][o]&(0x1<<(FONT_WIDTH-(l+1))))==0)
												{
														line[V_SCALE_FACTOR*o+n+2][m]=(line[5*o+n+2][m]<<2);
												}
										 
												else
												{
														line[V_SCALE_FACTOR*o+n+2][m]=((line[5*o+n+2][m]<<2)|3);
												}
												
												//send_data_uart('a');
												
											}
													
														
                    } 
                    
                    line[V_SCALE_FACTOR*o+n+2][m]=(line[5*o+n+2][m]<<2);
										
						if(setting_time==1)			
						{
								if(charUnderline[rowNum][m]=='1')
								{
										line[43][m]=0xffff;
									
								}
								else
									{
										line[43][m]=0x0000;
								}
						}
                }

            }
        }

}
