#include <stm32f091xc.h>
#include "gpio.h"


void config_GPIOA(void)
{
	
	//Enable GPIOA Clock
	RCC->AHBENR|=RCC_AHBENR_GPIOAEN;//Enable Clock for IO port A
	
			/************************  GPIO PINS for LED's************************/ 
	GPIOA->MODER|=GPIO_MODER_MODER8_0;//Select the Mode of PCA8 to General Purpose Output
	GPIOA->MODER&=~GPIO_MODER_MODER8_1;//Select the Mode of PA8 to General Purpose Output
	
	
//	/************************  UASRT4 Pins************************/ 
//	
	GPIOA->MODER|=GPIO_MODER_MODER0_1;//Select the Mode of PA0 to USART1
	GPIOA->MODER&=~GPIO_MODER_MODER0_0;//Select the Mode of PA0 to USART1
	
	GPIOA->MODER|=GPIO_MODER_MODER1_1;//Select the Mode of PA1 to USART1
	GPIOA->MODER&=~GPIO_MODER_MODER1_0;//Select the Mode of PA1 to USART1
	
	GPIOA->AFR[0]|=(1<<2U); //Set Alternate function 4 for A0 
	GPIOA->AFR[0]|=(1<<6U);//Set Alternate function 4 for A1
//	/************************  UASRT4 Pins************************/ 
//	
//	
//	/************************  MCO Pin************************/ 
//	
//	GPIOA->MODER|=GPIO_MODER_MODER8_1;// Select Alternate function mode for PA8
//	GPIOA->MODER&=~GPIO_MODER_MODER8_0;// Select Alternate function mode for PA8
//	/************************  MCO Pin************************/ 
//	
//	/************************  TSC Pins************************/ 
	//PA6-Electrode
	//PA7-Sampling capacitor
	GPIOA->MODER|=GPIO_MODER_MODER6_1;// Select Alternate function mode for PA6
	GPIOA->MODER&=~GPIO_MODER_MODER6_0;// Select Alternate function mode for PA6
	GPIOA->MODER|=GPIO_MODER_MODER7_1;// Select Alternate function mode for PA7
	GPIOA->MODER&=~GPIO_MODER_MODER7_0;// Select Alternate function mode for PA7
	
	GPIOA->AFR[0]|=(0x3<<24);// select alternate mode 3 for PA6 // for touch sensing
	GPIOA->AFR[0]|=(0x3<<28);// select alternate mode 3 for PA7 // for touch sensing
//	
//	
//	//PA2-Electrode
//	//PA3-Sampling capacitor
	GPIOA->MODER|=GPIO_MODER_MODER2_1;// Select Alternate function mode for PA2
	GPIOA->MODER&=~GPIO_MODER_MODER2_0;// Select Alternate function mode for PA2
	GPIOA->MODER|=GPIO_MODER_MODER3_1;// Select Alternate function mode for PA3
	GPIOA->MODER&=~GPIO_MODER_MODER3_0;// Select Alternate function mode for PA3
	
	GPIOA->AFR[0]|=(0x3<<8);// select alternate mode 3 for PA2 // for touch sensing
	GPIOA->AFR[0]|=(0x3<<12);// select alternate mode 3 for PA3 // for touch sensing
//	
//	/************************  TSC Pins************************/
	
	
}

void config_GPIOC(void)
{
	
	RCC->AHBENR|=RCC_AHBENR_GPIOCEN;//Enable Clock for IO port C
	
		/************************  GPIO PINS for LED's************************/ 
	GPIOC->MODER|=GPIO_MODER_MODER6_0;//Select the Mode of PC6 to General Purpose Output
	GPIOC->MODER&=~GPIO_MODER_MODER6_1;//Select the Mode of PC6 to General Purpose Output
	
	GPIOC->MODER|=GPIO_MODER_MODER7_0;//Select the Mode of PC7 to General Purpose Output
	GPIOC->MODER&=~GPIO_MODER_MODER7_1;//Select the Mode of PC7 to General Purpose Output
	
	GPIOC->MODER|=GPIO_MODER_MODER8_0;//Select the Mode of PC8 to General Purpose Output
	GPIOC->MODER&=~GPIO_MODER_MODER8_1;//Select the Mode of PC8to General Purpose Output
	
	GPIOC->MODER|=GPIO_MODER_MODER9_0;//Select the Mode of PC9 to General Purpose Output
	GPIOC->MODER&=~GPIO_MODER_MODER9_1;//Select the Mode of PC9 to General Purpose Output
		/************************  GPIO PINS for LED's************************/
}


void config_GPIOB(void)
{
	

	
	RCC->AHBENR|=RCC_AHBENR_GPIOBEN;//Enable Clock for IO port B	
	
	/************************  GPIO PINS for LED's************************/ 
	GPIOB->MODER|=GPIO_MODER_MODER9_0;//Select the Mode of PCA8 to General Purpose Output
	GPIOB->MODER&=~GPIO_MODER_MODER9_1;//Select the Mode of PA8 to General Purpose Output
	
	
		/************************  SPI1 Pins************************/ 
	GPIOB->MODER|=GPIO_MODER_MODER3_1;//Select the Mode of PB3 to SPI1
	GPIOB->MODER&=~GPIO_MODER_MODER3_0;//Select the Mode of PB3 to SPI1
	GPIOB->MODER|=GPIO_MODER_MODER4_0;//Select the Mode of PB4 to General Purpose Output for CS of SPI1 for LCD
	GPIOB->MODER&=~GPIO_MODER_MODER4_1;//Select the Mode of PB4 to General Purpose Output for CS of SPI1 for LCD
	GPIOB->MODER|=GPIO_MODER_MODER5_1;//Select the Mode of PB5 to SPI1
	GPIOB->MODER&=~GPIO_MODER_MODER5_0;//Select the Mode of PB5 to SPI1
		/************************  SPI1 Pins************************/ 
	
	
//	/************************  TSC Pins************************/ 
//	//PB0-Electrode
//	//PB1-Sampling capacitor
//	GPIOB->MODER|=GPIO_MODER_MODER0_1;// Select Alternate function mode for PB0
//	GPIOB->MODER&=~GPIO_MODER_MODER0_0;// Select Alternate function mode for PB0
//	GPIOB->MODER|=GPIO_MODER_MODER1_1;// Select Alternate function mode for PB1
//	GPIOB->MODER&=~GPIO_MODER_MODER1_0;// Select Alternate function mode for PB1
//	
//	GPIOB->AFR[0]|=(0x3<<0);// select alternate mode 3 for PB0 // for touch sensing
//	GPIOB->AFR[0]|=(0x3<<4);// select alternate mode 3 for PB1 // for touch sensing
//	
//	/************************  TSC Pins************************/

	
}
