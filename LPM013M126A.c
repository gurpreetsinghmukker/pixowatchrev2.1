#include "spi1.h"
#include "LPM013M126A.h"

int LCD_all_clear(void)
{
	LCD_CS_HIGH
	
		send_SPI_data(0x2000);

	LCD_CS_LOW
	
	return 0;
	
}

int LCD_blink_screen(uint8_t blackOrWhite)
{
	LCD_CS_HIGH
	if(blackOrWhite==BLACK)
	{
		send_SPI_data(M_BL_SCREEN);

	}
	
	else if(blackOrWhite==WHITE)
	{
		send_SPI_data(M_WH_SCREEN);
	}
	LCD_CS_LOW
	
	return 0;
}

int LCD_write_line(uint16_t lineAddress, uint16_t *ptrLine)
{
		LCD_CS_HIGH
		uint16_t dataPacket= (lineAddress|M_MONOCHROME);
		send_SPI_data(dataPacket);//Setting the Mode for the screen and the address of the line
		for(uint16_t i=0;i<12;i++)
		{
			send_SPI_data(*(ptrLine+i));
		}
		LCD_CS_LOW
		
	return 1;
}
