#include <stm32f091xc.h>
#include "systick.h"

void config_systick(void)
{
	SysTick->LOAD=0x0000BB80;
	SysTick->VAL=0;
	SysTick->CTRL=(SysTick->CTRL|(SysTick_CTRL_ENABLE_Msk)|(SysTick_CTRL_CLKSOURCE_Msk)|(SysTick_CTRL_TICKINT_Msk));//|(SysTick_CTRL_TICKINT_Msk));
}

