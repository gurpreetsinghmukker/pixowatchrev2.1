#ifndef USER_INTERFSCE_H_
#define USER_INTERFSCE_H_

#include <stdint.h>

#include "timer.h"
#include "uart1.h"
#include "string.h"
#include "rtc.h"


char is_user_command(char usr_cmd);
void user_input_parser(void);
uint8_t  touch_input(uint32_t electrode1, uint32_t electrode2);
	

#endif /*USER_INTERFSCE_H_*/
