#include "main.h"

#define HORIZONTAL_LINES 176
#define VERTICAL_LINES 176
#define H_LINES_IN_BLOCK 16
#define BLOCKS_IN_H_LINE ((HORIZONTAL_LINES)/(H_LINES_IN_BLOCK)) //11
#define V_LINES_IN_BLOCK 44
#define V_SCALE_FACTOR 5

volatile char command=0;
extern uint8_t  field_num;
volatile uint32_t AcquisitionValueG3;
volatile uint32_t AcquisitionValueG2;
volatile uint32_t AcquisitionValueG1;
volatile uint32_t  countertim=0;
volatile uint32_t  countertsc=0;
volatile uint8_t tpadstatus=0;

void TIM6_DAC_IRQHandler(void)
{
		
	countertim++;
	TIM6->SR&=~(TIM_SR_UIF);
	GPIOA->ODR^=(1<<8);
	update_LCD_time_date();
	draw_screen();
	TSC->CR|=TSC_CR_START;

}

void TSC_IRQHandler(void)
{
	
	countertsc++;
	GPIOB->ODR^=(1<<9U);
	if ((TSC->ISR & TSC_ISR_EOAF) == TSC_ISR_EOAF)
		{
			TSC->ICR = TSC_ICR_EOAIC; /* Clear flag */
			//AcquisitionValueG3 = TSC->IOGXCR[2]; /* Get G2 counter value */
			AcquisitionValueG2 = TSC->IOGXCR[1]; /* Get G2 counter value */
			AcquisitionValueG1 = TSC->IOGXCR[0]; /* Get G2 counter value */

		}
		
		tpadstatus=touch_input(AcquisitionValueG1,AcquisitionValueG2);
		send_data_uart(tpadstatus+48);
		send_data_uart(0x0D);
		send_data_uart(0x0A);
//		uint8_t thousands=AcquisitionValueG1/1000;
//		uint8_t hundreds=(AcquisitionValueG1%1000)/100;
//		uint8_t tens=((AcquisitionValueG1%1000)%100)/10;
//		uint8_t ones=((AcquisitionValueG1%1000)%100)%10;
//		send_data_uart(AcquisitionValueG2>>24);
//		send_data_uart(AcquisitionValueG2>>16);
//		send_data_uart(AcquisitionValueG2>>8);
//		send_data_uart(AcquisitionValueG2);
//		
//		send_data_uart(AcquisitionValueG1>>24);
//		send_data_uart(AcquisitionValueG1>>16);
//		send_data_uart(AcquisitionValueG1>>8);
//		send_data_uart(AcquisitionValueG1);
		
//		send_data_uart(thousands+48);
//		send_data_uart(hundreds+48);
//		send_data_uart(tens+48);
//		send_data_uart(ones+48);
//		send_data_uart(',');

}


void SysTick_Handler(void)
{

		SysTick->CTRL&=~SysTick_CTRL_COUNTFLAG_Msk;
	uint32_t systick_time= SysTick->VAL;
		if(timer>=20000)
		{
			timer=0;
		}
		else{
			timer++;
		}
	
}

void USART3_8_IRQHandler(void)
{
		command=recieve_data_uart();
}

int main(void)
{
	
	config_sys_clock();
	config_systick();
	config_GPIOA();
	config_GPIOB();
	config_TIM6();
	config_SPI1();
	SPI1_enable();
	config_UART4();
	config_TSC();
	
	initialise_RTC_clock(0x00073800,0x00006206);
	LCD_all_clear();

	NVIC_SetPriority(TSC_IRQn,2);
	NVIC_SetPriority(USART3_8_IRQn,0);
	NVIC_SetPriority(TIM6_DAC_IRQn,1);
	NVIC_EnableIRQ(TIM6_DAC_IRQn);
	NVIC_EnableIRQ(USART3_8_IRQn);
	NVIC_EnableIRQ(TSC_IRQn);
	while(1)
	{
		user_input_parser();
	}
	return 0;
}
