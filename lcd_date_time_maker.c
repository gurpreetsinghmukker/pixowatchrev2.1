#include <stm32f091xc.h>
#include "lcd_date_time_maker.h"


struct screen_modified //also defined in LCDapi.c
	{
		uint8_t row_num[4];
	};
extern char asciiLine[4][11];//in LCDapi.c
extern struct screen_modified* ptr_scr_mod_1;//in LCDapi.c

const char week_day_names[7][11]={{"  Monday   "},{"  Tuesday  "},{" Wednesday "},{"  Thursday "},{"   Friday  "},{"  Saturday "},{"   Sunday  "}};
const char month_names[12][3]={{"JAN"},{"FEB"},{"MAR"},{"APR"},{"MAY"},{"JUN"},{"JUL"},{"AUG"},{"SEP"},{"OCT"},{"NOV"},{"DEC"}};


void update_LCD_time_date(void)
{
	/* Varibles to store current RTC register values when changing the screen*/
	static uint32_t trCopy=0;
	static uint32_t drCopy=0;
	uint8_t ones=0;
  uint8_t tens=0;
	
//	if(drCopy!=RTC->DR)
//	{
		drCopy=RTC->DR;
	/*Update Day*/
	uint8_t day_name=1;
	uint8_t month_name=1;
	
	//set the name of the day
	day_name=((RTC->DR&(RTC_DR_WDU_0|RTC_DR_WDU_1|RTC_DR_WDU_2))>>13);
	memcpy(&asciiLine[0][0],&week_day_names[day_name-1][0],11);
		
	//Set the date
	ones=drCopy&(RTC_DR_DU_0|RTC_DR_DU_1|RTC_DR_DU_2|RTC_DR_DU_3);
	tens=((drCopy&(RTC_DR_DT_0|RTC_DR_DT_1))>>4);
	asciiLine[1][4]='0'+tens;
	asciiLine[1][5]='0'+ones;
		
	//Set the month name
	ones=((drCopy&(RTC_DR_MU_0|RTC_DR_MU_1|RTC_DR_MU_2|RTC_DR_MU_3))>>8);
	tens=((drCopy&(RTC_DR_MT))>>12);
	month_name=tens*10+ones;
	memcpy(&asciiLine[1][0],&month_names[month_name-1][0],3);
	
	//Set the Year
	ones=(drCopy&(RTC_DR_YU_0|RTC_DR_YU_1|RTC_DR_YU_2|RTC_DR_YU_3))>>16;
	tens=(drCopy&(RTC_DR_YT_0|RTC_DR_YT_1|RTC_DR_YT_2|RTC_DR_YT_3))>>20;
	asciiLine[1][9]='0'+tens;
	asciiLine[1][10]='0'+ones;
	
//	ptr_scr_mod_1->row_num[0]=1;
//	ptr_scr_mod_1->row_num[1]=1;
	
//	}
	
//	if(trCopy!=RTC->TR)
//	{
		trCopy=RTC->TR;

	//set the seconds
	ones=trCopy&(RTC_TR_SU_0|RTC_TR_SU_1|RTC_TR_SU_2|RTC_TR_SU_3);
	tens=((trCopy&(RTC_TR_ST_0|RTC_TR_ST_1|RTC_TR_ST_2))>>4);
	asciiLine[2][6]='0'+tens;
	asciiLine[2][7]='0'+ones;
	
	//set the minutes
	ones=(trCopy&(RTC_TR_MNU_0|RTC_TR_MNU_1|RTC_TR_MNU_2|RTC_TR_MNU_3))>>8;
	tens=((trCopy&(RTC_TR_MNT_0|RTC_TR_MNT_1|RTC_TR_MNT_2))>>12);
	asciiLine[2][3]='0'+tens;
	asciiLine[2][4]='0'+ones;	
	
	
	//set the hours

	ones=(trCopy&(RTC_TR_HU_0|RTC_TR_HU_1|RTC_TR_HU_2|RTC_TR_HU_3))>>16;
	tens=((trCopy&(RTC_TR_HT_0|RTC_TR_HT_1))>>20);
	asciiLine[2][0]='0'+tens;
	asciiLine[2][1]='0'+ones;	

	//set the AM/PM
	if(((trCopy&RTC_TR_PM)>>22)==1)
	{
		asciiLine[2][9]='P';
	}
	else
	{
		asciiLine[2][9]='A';
	}
	
//	ptr_scr_mod_1->row_num[2]=1;
//}
	
}
