#include <stm32f091xc.h>
#include "rtc.h"

void rmv_RTC_write_protection()
{
	PWR->CR|=(1<PWR_CR_DBP);// Backup domain register write protection Disabled
	
	RTC->WPR = 0xCA;//Entering KEY into Write Protection Register
	RTC->WPR = 0x53;//Entering KEY into Write Protection Register
}

void initialise_RTC_clock(uint32_t time_reg,uint32_t date_reg)
{
	
	//*********************Configuration of LSI Clock for RTC*************************************//
	RCC->APB1ENR|=(RCC_APB1ENR_PWREN);//Clock enabled for Power Interface peripheral
	RCC->CSR|=(RCC_CSR_LSION);// Enable the LSE Clock
	
	while(1) // Wait for the the LSE clock to get stabalized																	
	{
		if((RCC->CSR & RCC_CSR_LSIRDY) != 0)
		{
			break;
		}
	}
	RCC->BDCR|=(RCC_BDCR_BDRST);//Reset the Backup domain registers
	RCC->BDCR&=~(RCC_BDCR_BDRST);// NOT-reset the Backup domain register reset bit//check the reference manual 
	PWR->CR|=(PWR_CR_DBP);//Disable Backup domain protection
	
	
	RCC->BDCR&=~(RCC_BDCR_RTCSEL_LSE);
	RCC->BDCR|=RCC_BDCR_RTCSEL_LSI ;// LSE selected as RTC Clock
	RCC->BDCR|=(RCC_BDCR_RTCEN);// Enable the RTC prepheral
	//***********************************************************************************************//
	
	rmv_RTC_write_protection();
	RTC->BKP0R|=0x1;
	RTC->ISR|=RTC_ISR_INIT;//Entering the Initialization mode of the RTC
	
	while(1)//Polling to check if the RTC has enterd the initialization mode
	{
		if((RTC->ISR&RTC_ISR_INITF)!=0)
		{
			break;
		}
	}
	//RTC->TR|=(1<<22);
	RTC->TR=time_reg;
	RTC->DR=date_reg;
	//RTC->PRER is already set to the default values to generate 1Hz clock
	
	
	
	RTC->ISR&=~RTC_ISR_INIT;// Exits out of the Initialization mode and starts the RTC counter
	
}
