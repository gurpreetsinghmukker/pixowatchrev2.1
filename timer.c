#include <stm32f091xc.h>
#include "timer.h"

#define ROLLOVER_INTERVAL 20000

volatile uint16_t timer=0;

void config_TIM6(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM6EN;////Enable Clock for TIM6
	TIM6->PSC=2799;
	TIM6->ARR=1999;
	TIM6->CR1|=(TIM_CR1_ARPE);

	TIM6->DIER|=(TIM_DIER_UIE);
	
	TIM6->CR1|=(TIM_CR1_CEN);
	
	
}

void Delay(uint16_t amount)
{
  uint16_t start = timer;
  uint16_t elapsed;
  uint16_t rolled_over = 0;
  uint16_t last_time = start;

  do
  {
    int timer_reg = timer;
    // Check for rollover
    if(last_time > timer_reg)
		{
      // ROLLOVER_INTERVAL is the magic number at which the timer rolls over to 0
      rolled_over += ROLLOVER_INTERVAL - start;
      start = 0;
    }
    last_time = timer_reg;
		
    if(rolled_over == 0) 
		{
      elapsed = timer_reg - start;
    } 
		else 
		{
      elapsed = timer_reg + rolled_over;
    }
  } while (elapsed < amount);
}
