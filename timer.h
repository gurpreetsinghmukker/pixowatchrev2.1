#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>

extern volatile uint16_t timer;

void config_TIM6(void);
void Delay(uint16_t amount);

#endif /*TIMER_H_*/
