#ifndef LCDAPI_H_
#define LCDAPI_H_

#include <stdint.h>
#include <string.h>

#define HORIZONTAL_LINES 176
#define VERTICAL_LINES 176
#define H_LINES_IN_BLOCK 16
#define BLOCKS_IN_H_LINE ((HORIZONTAL_LINES)/(H_LINES_IN_BLOCK)) //11
#define V_LINES_IN_BLOCK 44
#define V_SCALE_FACTOR 5

#define _ROW(x) x-1
#define _BLOCK(x) x-1




void draw_screen(void);
void refresh_row(uint8_t rowNum);// Makes the 2D array for the row using makeRowArray() function and then refreshes the row on the screen(sends data to the screen for the full row)
void make_row_array(uint8_t rowNum);// Computes the full 2D row array based on the parameter rowNum given by the user and the data present in the asciiLine character array. 
void update_character(char character, uint8_t rowNum, uint8_t BlockNum);//updates individual character on the screen
void update_LCD_time_date(void);
	

#endif /*LCDAPI_H_*/
