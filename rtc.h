#ifndef RTC_H_
#define RTC_H_

#include <stdint.h>


void rmv_RTC_write_protection(void);
void initialise_RTC_clock(uint32_t time_reg,uint32_t date_reg);

#endif /*RTC_H_*/
