#ifndef MAIN_H_
#define MAIN_H_

#include <stdint.h>
#include <stm32f091xc.h>
#include "clock.h"
#include "gpio.h"
#include "spi1.h"
#include "uart1.h"
#include "timer.h"
#include "tsc.h"
#include "rtc.h"
#include "LPM013M126A.h"
#include "LCDapi.h"
#include "systick.h"
#include "user_interface.h"
#include "string.h"

#endif /*MAIN_H_*/
