#include <stm32f091xc.h>
#include "user_interface.h"

//#define REGDR
//#define REGTR
//#define LOOPDRTR

#define TSC_PADG1_LOW_THRESHOLD 3000	
#define TSC_PADG1_HIGH_THRESHOLD 3100

#define TSC_PADG2_LOW_THRESHOLD 4000	
#define TSC_PADG2_HIGH_THRESHOLD 4100

#define T_NOPAD 0
#define T_PAD1_ONLY 1
#define T_PAD2_ONLY 2
#define T_PAD1_PAD2 3


extern volatile char command;// defined in main.c in usart IRQ handler function
extern char asciiLine1[4][11];
extern char week_day_names[7][11];
extern uint8_t underline[8][5];
extern char month_names[12][3];
extern uint8_t  field_num;
extern uint8_t tpadstatus;

uint8_t setting_time=0;


uint8_t  touch_input(uint32_t electrode1, uint32_t electrode2)
{
	uint8_t touchpads=0;
	if(electrode1<TSC_PADG1_LOW_THRESHOLD)
	{
		touchpads|=(1<<0); //BIT 0 is for PAD1
	}
	else if(electrode1>TSC_PADG1_HIGH_THRESHOLD)
	{
		touchpads&=~(1<<0);//BIT 0 is for PAD1
	}
	
	if(electrode2<TSC_PADG2_LOW_THRESHOLD)
	{
		touchpads|=(1<<1);//BIT 1 is for PAD2
	}
		else if(electrode1>TSC_PADG2_HIGH_THRESHOLD)
	{
		touchpads&=~(1<<1);//BIT 1 is for PAD2
	}
	return touchpads;
}

struct screen_modified //also defined in LCDapi.c
	{
		uint8_t row_num[4];
	};
extern struct screen_modified* ptr_scr_mod_1;//in LCDapi.c

char is_user_command(char usr_cmd)
{
	if(usr_cmd==command)
	{
		return 1;	
	}
	else
	{
		return 0;
	} 
}

void user_input_parser(void)
{
			//if(is_user_command('e'))// enter the time setting mode
	if(tpadstatus==3)	
	{
		Delay(1000);
		send_string("in",2);
		setting_time=1;
		modify_char_underline(0);
		if(tpadstatus==3)
		{
		while(tpadstatus>0)
		{
		}

			
			//Delay(2000);
			
			uint32_t user_tr=0x00;
			uint32_t user_dr=0x00;
			uint8_t dr_tr_num[8]={0};
			uint8_t day_name=1;
			uint8_t month_name=1;
			uint8_t ones=0;
			uint8_t tens=0;
			uint32_t drCopy=RTC->DR;
			uint32_t trCopy=RTC->TR;
			
			//set the name of the day
			day_name=((drCopy&(RTC_DR_WDU_0|RTC_DR_WDU_1|RTC_DR_WDU_2))>>13);
			dr_tr_num[0]=day_name;
				
			//Set the date
			ones=drCopy&(RTC_DR_DU_0|RTC_DR_DU_1|RTC_DR_DU_2|RTC_DR_DU_3);
			tens=((drCopy&(RTC_DR_DT_0|RTC_DR_DT_1))>>4);
			dr_tr_num[2]=tens*10+ones;
			
			//Set the month name
			ones=((drCopy&(RTC_DR_MU_0|RTC_DR_MU_1|RTC_DR_MU_2|RTC_DR_MU_3))>>8);
			tens=((drCopy&(RTC_DR_MT))>>12);
			dr_tr_num[1]=(tens*10+ones);
			
			//Set the Year
			ones=(drCopy&(RTC_DR_YU_0|RTC_DR_YU_1|RTC_DR_YU_2|RTC_DR_YU_3))>>16;
			tens=(drCopy&(RTC_DR_YT_0|RTC_DR_YT_1|RTC_DR_YT_2|RTC_DR_YT_3))>>20;
			dr_tr_num[3]=tens*10+ones;
			
			//set the seconds
			ones=trCopy&(RTC_TR_SU_0|RTC_TR_SU_1|RTC_TR_SU_2|RTC_TR_SU_3);
			tens=((trCopy&(RTC_TR_ST_0|RTC_TR_ST_1|RTC_TR_ST_2))>>4);
			dr_tr_num[6]=tens*10+ones;
			
			//set the minutes
			ones=(trCopy&(RTC_TR_MNU_0|RTC_TR_MNU_1|RTC_TR_MNU_2|RTC_TR_MNU_3))>>8;
			tens=((trCopy&(RTC_TR_MNT_0|RTC_TR_MNT_1|RTC_TR_MNT_2))>>12);
			dr_tr_num[5]=tens*10+ones;
			
			//set the hours

			ones=(trCopy&(RTC_TR_HU_0|RTC_TR_HU_1|RTC_TR_HU_2|RTC_TR_HU_3))>>16;
			tens=((trCopy&(RTC_TR_HT_0|RTC_TR_HT_1))>>20);
			dr_tr_num[4]=tens*10+ones;
			
			//set the AM/PM
			if(((trCopy&RTC_TR_PM)>>22)==1)
			{
				dr_tr_num[7]=1;
			}
			else
			{
				dr_tr_num[7]=0;
			}
			
//			send_string("e",1);
			
			do
			{
				
				while(tpadstatus>0)
						{
						}
				
				user_tr= (dr_tr_num[7]<<22)|((dr_tr_num[6]/10)<<4)|((dr_tr_num[6]%10)<<0)|((dr_tr_num[5]/10)<<12)|((dr_tr_num[5]%10)<<8)|((dr_tr_num[4]/10)<<20)|((dr_tr_num[4]%10)<<16);
				user_dr=((dr_tr_num[2])%10<<0)|((dr_tr_num[2])/10<<4)|((dr_tr_num[1])%10<<8)|((dr_tr_num[1])/10<<12)|((dr_tr_num[0])<<13)|(dr_tr_num[3]%10<<16)|(dr_tr_num[3]/10<<20);
				
				#ifdef REGDR 
				
				send_data_uart(user_dr>>24);
				send_data_uart(user_dr>>16);
				send_data_uart(user_dr>>8);
				send_data_uart(user_dr);
				#endif
							
				#ifdef REGTR 
				send_data_uart(user_tr);
				send_data_uart(user_tr>>8);
				send_data_uart(user_tr>>16);
				send_data_uart(user_tr>>24);
				#endif

				#ifdef LOOPDRTR
				for(uint8_t j=0; j<8;j++)
				{
					send_data_uart(dr_tr_num[j]);
				}
				
				#endif
				for(uint8_t i=0; i<8;i++)// repeating nine time for each field on the screen
				{

					//if(is_user_command('x'))
					if(tpadstatus==3)
					{
						while(tpadstatus>0)
						{
						}
						send_string("break",5);
						goto out_of_here;
						break;
					}
					while(tpadstatus>0)
					{
					}
//					send_string("for",3);
					uint8_t counter=dr_tr_num[i];
					modify_char_underline(i);
					do
					{
								
						//if(is_user_command('i'))
						if(tpadstatus==1)
						{
							Delay(200);
							tpadstatus=0;

//							send_string("i",1);
							if(counter==underline[i][4])
							{
								counter=underline[i][3];
							}
							else
							{
								counter++;
							}
							
		
							switch(i)
							{
								
								case 0:
									dr_tr_num[i]=counter;
									memcpy(&asciiLine1[0][0],&week_day_names[counter-1][0],11);
									break;
								
								case 1:
									memcpy(&asciiLine1[1][0],&month_names[counter-1][0],3);
									dr_tr_num[i]=counter;
									break;	
									
								case 7:
									if((counter)==1)
									{
										asciiLine1[2][9]='P';
									}
									else
									{
										asciiLine1[2][9]='A';
									}
									dr_tr_num[i]=counter;
									break;	
									
								default:
									dr_tr_num[i]=counter;
									send_data_uart(counter);
									asciiLine1[underline[i][0]][underline[i][1+1]]=((counter)%10)+48;
									asciiLine1[underline[i][0]][underline[i][1]]=((counter)/10)+48;
									break;
							}
							
							#ifdef LOOPDRTR
							for(uint8_t j=0; j<8;j++)
							{
								send_data_uart(dr_tr_num[j]);
							}
							
							#endif
							
							user_tr= (dr_tr_num[7]<<22)|((dr_tr_num[6]/10)<<4)|((dr_tr_num[6]%10)<<0)|((dr_tr_num[5]/10)<<12)|((dr_tr_num[5]%10)<<8)|((dr_tr_num[4]/10)<<20)|((dr_tr_num[4]%10)<<16);
							user_dr=((dr_tr_num[2])%10<<0)|((dr_tr_num[2])/10<<4)|((dr_tr_num[1])%10<<8)|((dr_tr_num[1])/10<<12)|((dr_tr_num[0])<<13)|(dr_tr_num[3]%10<<16)|(dr_tr_num[3]/10<<20);
							#ifdef REGDR
							
							send_data_uart(user_dr>>24);
							send_data_uart(user_dr>>16);
							send_data_uart(user_dr>>8);
							send_data_uart(user_dr);

							#endif
							
							#ifdef REGTR 
							
							send_data_uart(user_tr);
							send_data_uart(user_tr>>8);
							send_data_uart(user_tr>>16);
							send_data_uart(user_tr>>24);
							#endif
										
							for(uint8_t j=0; j<4;j++)
							{
										
								for(uint8_t i=0; i<11; i++)
									{
										send_data_uart(asciiLine1[j][i]);
									}
									
								send_data_uart(0x0D);
								send_data_uart(0x0A);
							}

							send_data_uart(counter+48);
							send_data_uart(0x0D);
							send_data_uart(0x0A);
						}
						command=0;
					//}while(!(is_user_command('n')) && !(is_user_command('x')));
					}while((tpadstatus!=2)&&(tpadstatus!=3));
					
					send_string("out",3);
				}
				//send_string("out of for",10);
			//}while(!(is_user_command('x')));
			}while(tpadstatus!=3);
				#ifdef LOOPDRTR
			for(uint8_t j=0; j<8;j++)
			{
				send_data_uart(dr_tr_num[j]);
			}
			
			#endif
			#ifdef REGDR
							

			send_data_uart(user_dr>>24);
			send_data_uart(user_dr>>16);
			send_data_uart(user_dr>>8);
			send_data_uart(user_dr);
						
			#endif
							
			#ifdef REGTR 
			
			send_data_uart(user_tr);
			send_data_uart(user_tr>>8);
			send_data_uart(user_tr>>16);
			send_data_uart(user_tr>>24);
			#endif
			out_of_here:
			//send_string("out of e",8);
			rmv_RTC_write_protection();
			initialise_RTC_clock(user_tr,user_dr);
			
//			ptr_scr_mod_1->row_num[0]=1;
//			ptr_scr_mod_1->row_num[1]=1;
//			ptr_scr_mod_1->row_num[2]=1;
//			ptr_scr_mod_1->row_num[3]=1;
			setting_time=0;
			clear_charunderline();
		}
	}
}