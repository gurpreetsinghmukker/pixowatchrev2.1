#include <stm32f091xc.h>
#include "tsc.h"

void config_TSC(void)
{
	
	/* Configure TCS */
	/* (1) Enable TSC Clock */
	/* With a charge transfer around 2.5 �s */
	/* (2) Select fPGCLK = fHCLK/32,
			Set pulse high = 2xtPGCLK,Master
			Set pulse low = 2xtPGCLK
			Set Max count value = 16383 pulses
			Enable TSC */
	/* (3) Disable hysteresis */
	/* (4) Enable end of acquisition IT */
	/* (5) Sampling enabled, G3IO3 */
	/* (6) Channel enabled, G3IO2 */
	/* (7) Enable group, G3 */
	
	RCC->AHBENR|=RCC_AHBENR_TSCEN;/* (1) */
	TSC->CR = TSC_CR_PGPSC_2 | TSC_CR_PGPSC_0 | TSC_CR_CTPH_1 | TSC_CR_CTPL_3| TSC_CR_MCV_2 | TSC_CR_MCV_1 | TSC_CR_TSCE; /* (2) */
	
//	TSC->IOHCR &= (uint32_t)(~(TSC_IOHCR_G3_IO3 | TSC_IOHCR_G3_IO2)); /* (3) */
	
	TSC->IOHCR &= (uint32_t)(~(TSC_IOHCR_G2_IO4 | TSC_IOHCR_G2_IO3)); /* (3) */
	
	TSC->IOHCR &= (uint32_t)(~(TSC_IOHCR_G1_IO3 | TSC_IOHCR_G1_IO4)); /* (3) */
	
	TSC->IER = TSC_IER_EOAIE; /* (4) */
	
//	TSC->IOSCR |= TSC_IOSCR_G3_IO3; /* (5) */
//	TSC->IOCCR |= TSC_IOCCR_G3_IO2; /* (6) */
//	TSC->IOGCSR |= TSC_IOGCSR_G3E; /* (7) */
	
	TSC->IOSCR |= TSC_IOSCR_G2_IO4; /* (5) */
	TSC->IOCCR |= TSC_IOCCR_G2_IO3; /* (6) */
	TSC->IOGCSR |= TSC_IOGCSR_G2E; /* (7) */
	
	TSC->IOSCR |= TSC_IOSCR_G1_IO3; /* (5) */
	TSC->IOCCR |= TSC_IOCCR_G1_IO4; /* (6) */
	TSC->IOGCSR |= TSC_IOGCSR_G1E; /* (7) */
	
}