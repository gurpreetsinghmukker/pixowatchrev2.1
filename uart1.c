#include <stm32f091xc.h>
#include "uart1.h"

void config_UART4(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_USART4EN;//Enable Clock for UART4
	
	USART4->BRR = 48000000 / 115200; /* (1) */
	USART4->CR1 = USART_CR1_TE | USART_CR1_RE|USART_CR1_UE|USART_CR1_RXNEIE;
}

void send_data_uart(uint8_t data)
{
	USART4->TDR=data;
	uart_wait();
}

void send_string(char string[],uint8_t size)
{
	int i=0;
	for (i=0;i<size;i++)
	{
		send_data_uart(string[i]);
	}
	
	send_data_uart(0x0D);
	send_data_uart(0x0A);
	
}
void uart_wait(void)
{
	while ((USART4->ISR & USART_ISR_TXE)==0)
	{
		
	}
}

uint8_t recieve_data_uart(void)
{
	while((USART4->ISR & USART_ISR_RXNE)==0)
	{
	}
	return  USART4->RDR;
		
}
