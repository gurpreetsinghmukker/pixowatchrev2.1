#ifndef LPM013M126A_H_
#define LPM013M126A_H_

#include <stdint.h>
#include <stm32f091xc.h>

// Modes are slected with M0- M5 ; five bits
#define M_BL_SCREEN    0x3000
#define M_WH_SCREEN    0x3800
#define M_MONOCHROME   0x8800
#define M_COLOUR_DUMMY 0x9000



//
#define BLACK 0
#define WHITE 1

#define LCD_CS_HIGH GPIOB->ODR|=(1<<4U);
#define LCD_CS_LOW  GPIOB->ODR&=~(1<<4U);



int LCD_all_clear(void);

int LCD_blink_screen(uint8_t blackOrWhite);// Once this function is used, LCD stops accepting other Commands. Unless LCDAllClear() function is used. 

int LCD_write_line(uint16_t lineAddress, uint16_t *ptrLine);

int LCD_write_multipleLines(uint16_t startLineAddr, uint16_t endLineAddr, uint16_t * linesData);

int LCD_write_frame(uint8_t monoOrRGB ,int16_t * frameData);

#endif /*LPM013M126A_H_*/


